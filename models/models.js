var mongoose = require('mongoose');

let usersSchema = require('./schemas/users');

exports.users = mongoose.model('users', usersSchema, 'users');