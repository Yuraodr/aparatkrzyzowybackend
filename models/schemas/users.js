var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var passportLocalMongoose = require('passport-local-mongoose');

var users = new Schema({
    user: String,
    pass: String,
    played: 
    {
        type: Number, 
        "default": 0
    },    
    points: 
    {
        type: Number, 
        "default": 0
    },
});

users.plugin(passportLocalMongoose);

module.exports = mongoose.model('users', users);
